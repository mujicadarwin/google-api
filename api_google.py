import requests
import googlemaps
import time
import json

def place_search():
    api_key = 'AIzaSyD8p8sVaZC04ECr6U3CpIP9Oif0vlxvRyc'
    endpoint_url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json"
    gmaps = googlemaps.Client(key=api_key)
    places = []
    params = {
    'location':'53.5363648,-113.5068109',
    'radius':'10000',
    'types':'restaurant',
    'key':api_key}

    res = requests.get(endpoint_url, params= params)
    results = json.loads(res.content)
    # pull places lista
    places.extend(results['results'])
    time.sleep(3)
    while  "next_page_token" in results:
        params['pagetoken'] = results['next_page_token']
        res = requests.get(endpoint_url, params=params)
        results = json.loads(res.content)
        places.extend(results['results'])
        print(places)
        time.sleep(3)
    
    return places

def get_place_details():
    api_key = 'AIzaSyD8p8sVaZC04ECr6U3CpIP9Oif0vlxvRyc'
    endpoint_url = "https://maps.googleapis.com/maps/api/place/details/json"
    fields = ['name', 'formatted_address', 'international_phone_number', 'website', 'rating']
    params = {
    'pladeid': place_id,
    'fields':",".join(fields),
    'key':api_key}
    res = requests.get(endpoint_url,params=params)
    place_details = json.loads(res.content)
     
    return place_details




if __name__ == '__main__':
    place_search()




